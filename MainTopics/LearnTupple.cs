﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\22_Professional-CSharp\MainTopics\LearnTupple.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

namespace YouSoft.Vn.ProCSharp.MainTopic
{
    public class LearnTuple
    {
        public static void Test()
        {
            PredefinedTuple();
        }

        static void AnonymousTuple()
        {
            var x = (2, 4, 5, 6);
            Console.WriteLine(x.Item4);
        }

        static List<Tuple<int, string, double>> datum = new List<Tuple<int, string, double>>();

        static void NamedTuple()
        {
            var x = (Name: "Si", LastName: "Dong");
            Console.WriteLine(x.Name);
        }

        static void PredefinedTuple()
        {
            datum.Add(new Tuple<int, string, double>(1, "Nam", 500));
            datum.Add(new Tuple<int, string, double>(2, "Si", 200));
            var sum = datum
                .Where(item => item.Item3 > 200)
                          .Sum(item => item.Item3);
            Console.WriteLine(sum);
        }
    }
}
