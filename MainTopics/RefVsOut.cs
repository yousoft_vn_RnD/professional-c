﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\LearnCsharp\MainTopics\RefVsOut.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Linq;

namespace YouSoft.Vn.ProCSharp.MainTopic
{
    public class TestData
    {
        public double X { get; set; }
    }


    public class RefVsOut
    {
        public static void Test()
        {
            TestByObject();
        }

        static void TestByObject()
        {
            var obj = new TestData { X = 50 };
            ByObject(in obj);
            Console.WriteLine($"After: {obj.X}");
        }

        static void ByObject(in TestData testData)
        {
            Console.WriteLine($"Before: {testData.X}");
            testData.X = 100;
        }

        static void TestByValue()
        {
            var x = 5d;
            ByValue(ref x);
            Console.WriteLine($"Number after changed: {x}");
        }

        static void ByValue(ref double x)
        {
            Console.WriteLine($"Your number is:{x}");
            x = 100;
        }

        static void tryToDouble()
        {
            do
            {
                Console.WriteLine("Please enter a number:");
                var text = Console.ReadLine();
                if(text == "quit")
                    break;

                var x = 0d;

                if(double.TryParse(text, out x))
                {
                    Console.WriteLine($"Your formatted number is: {x:N2}");
                } else
                {
                    Console.WriteLine("Your input is not a number");
                }
            } while (true);
        }
    }
}
